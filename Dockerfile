FROM openjdk:11-jre-slim
LABEL maintainer="Christopher Ocares H."

#Crea un directorio para la aplicacion (JAR)
RUN mkdir -p /opt/logs
WORKDIR /opt

#Copia la aplicacion(JAR) al directorio
ADD challenge-ms-exchange-api/target/*.jar /opt/app.jar

#Expone el puerto de la aplicacion en el contenedor
EXPOSE 8080

#Ejecucion de la aplicacion
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -XshowSettings:vm\
                -Duser.timezone=America/Lima  \
                -Dspring.profiles.active=$PROFILE \
                -jar app.jar"]
