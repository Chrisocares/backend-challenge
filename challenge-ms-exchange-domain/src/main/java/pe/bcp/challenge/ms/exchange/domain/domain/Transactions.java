package pe.bcp.challenge.ms.exchange.domain.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANSACTIONS")
@Getter
@Setter
public class Transactions {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String currencyOrigin;
  private String currencyDestination;
  private Double amountCurrencyOrigin;
  private Double amountCalculated;
  private Double rateExchange;
  private String channel;
  private String dateTimeTransaction;
}
