package pe.bcp.challenge.ms.exchange.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class NewRateExchangeResult {
  private String currencyOrigin;
  private String currencyDestination;
  private Double amountExchange;
  private String dateTimeUpdate;
  private String description;
  private String channel;
}
