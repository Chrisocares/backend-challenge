package pe.bcp.challenge.ms.exchange.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.bcp.challenge.ms.exchange.domain.domain.CurrencyExchange;

public interface CurrencyExchangeDAO extends JpaRepository<CurrencyExchange, Long> {

  CurrencyExchange findByCurrencyOriginAndCurrencyDestination (String currencyOrigin, String currencyDestination);
}
