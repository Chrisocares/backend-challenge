package pe.bcp.challenge.ms.exchange.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class NewRateExchangeDTO {
  private String currencyOrigin;
  private String currencyDestination;
  private Double amountExchange;
  private String symbolCurrency;
  private String dateTimeUpdate;
  private String description;
  private String strategy;
  private String channel;
}
