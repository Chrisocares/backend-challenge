package pe.bcp.challenge.ms.exchange.domain.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CURRENCYEXCHANGE")
@Getter
@Setter
public class CurrencyExchange {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String currencyOrigin;
  private String currencyDestination;
  private Double amountExchange;
  private String symbolCurrency;
  private String dateTimeUpdate;
  private String description;
  private String strategy;
  private String channel;
}
