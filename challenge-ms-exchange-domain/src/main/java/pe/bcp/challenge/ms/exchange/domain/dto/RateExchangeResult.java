package pe.bcp.challenge.ms.exchange.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RateExchangeResult {
  private double amountOrigin;
  private double amountExchange;
  private String currencyOrigin;
  private String currencyDestination;
  private double rateExchange;
}
