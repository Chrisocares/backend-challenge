package pe.bcp.challenge.ms.exchange.domain.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.bcp.challenge.ms.exchange.domain.domain.Transactions;

public interface TransactionsDAO extends JpaRepository<Transactions, Long> {
}
