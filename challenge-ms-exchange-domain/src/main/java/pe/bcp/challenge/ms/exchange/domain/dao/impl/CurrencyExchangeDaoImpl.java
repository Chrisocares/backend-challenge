package pe.bcp.challenge.ms.exchange.domain.dao.impl;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.bcp.challenge.ms.exchange.domain.dao.CurrencyExchangeDAO;
import pe.bcp.challenge.ms.exchange.domain.dao.TransactionsDAO;
import pe.bcp.challenge.ms.exchange.domain.domain.CurrencyExchange;
import pe.bcp.challenge.ms.exchange.domain.domain.Transactions;
import pe.bcp.challenge.ms.exchange.domain.dto.NewRateExchangeDTO;
import pe.bcp.challenge.ms.exchange.domain.dto.NewRateExchangeResult;
import pe.bcp.challenge.ms.exchange.domain.dto.RateExchangeDTO;
import pe.bcp.challenge.ms.exchange.domain.dto.RateExchangeResult;

import javax.persistence.EntityNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Repository
@Transactional
public class CurrencyExchangeDaoImpl {

  @Autowired
  CurrencyExchangeDAO currencyExchangeDAO;

  @Autowired
  TransactionsDAO transactionsDAO;

  public Single<RateExchangeResult> calculateExchangeRate(RateExchangeDTO rateExchangeDTO){
    return Single.create(singleSubscriber -> {
      CurrencyExchange currencyExchange = currencyExchangeDAO.findByCurrencyOriginAndCurrencyDestination(rateExchangeDTO.getCurrencyOrigin(),rateExchangeDTO.getCurrencyDestination());
      if (currencyExchange == null) {
        singleSubscriber.onError(new EntityNotFoundException());
      }
      Double amountCalculated = 0.00;
      if (currencyExchange.getStrategy().equals("c=a*b"))
        amountCalculated = rateExchangeDTO.getAmountExchange() * currencyExchange.getAmountExchange();
      else if (currencyExchange.getStrategy().equals("c=a/b")) {
        amountCalculated = rateExchangeDTO.getAmountExchange() / currencyExchange.getAmountExchange();
      }
      RateExchangeResult rateExchangeResult = new RateExchangeResult();
      rateExchangeResult.setAmountExchange(amountCalculated);
      rateExchangeResult.setAmountOrigin(rateExchangeDTO.getAmountExchange());
      rateExchangeResult.setCurrencyDestination(rateExchangeDTO.getCurrencyDestination());
      rateExchangeResult.setCurrencyOrigin(rateExchangeDTO.getCurrencyOrigin());
      rateExchangeResult.setRateExchange(currencyExchange.getAmountExchange());

      String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
      Transactions transactions = new Transactions();
      transactions.setAmountCalculated(rateExchangeResult.getAmountExchange());
      transactions.setAmountCurrencyOrigin(rateExchangeDTO.getAmountExchange());
      transactions.setChannel(rateExchangeDTO.getChannel());
      transactions.setDateTimeTransaction(timeStamp);
      transactions.setRateExchange(rateExchangeResult.getRateExchange());
      transactions.setCurrencyOrigin(rateExchangeDTO.getCurrencyOrigin());
      transactions.setCurrencyDestination(rateExchangeDTO.getCurrencyDestination());
      transactionsDAO.save(transactions);

      singleSubscriber.onSuccess(rateExchangeResult);
    });
  }

  public Single<NewRateExchangeResult> createNewExchangeRate(NewRateExchangeDTO newRateExchangeDTO){
    return Single.create(singleSubscriber -> {
      CurrencyExchange currencyExchange = new CurrencyExchange();
      currencyExchange.setAmountExchange(newRateExchangeDTO.getAmountExchange());
      currencyExchange.setChannel(newRateExchangeDTO.getChannel());
      currencyExchange.setCurrencyDestination(newRateExchangeDTO.getCurrencyDestination());
      currencyExchange.setCurrencyOrigin(newRateExchangeDTO.getCurrencyOrigin());
      currencyExchange.setDescription(newRateExchangeDTO.getDescription());
      currencyExchange.setStrategy(newRateExchangeDTO.getStrategy());
      currencyExchange.setSymbolCurrency(newRateExchangeDTO.getSymbolCurrency());
      currencyExchange.setDateTimeUpdate(newRateExchangeDTO.getDateTimeUpdate());

      NewRateExchangeResult newRateExchangeResult = new NewRateExchangeResult();
      newRateExchangeResult.setAmountExchange(newRateExchangeDTO.getAmountExchange());
      newRateExchangeResult.setChannel(newRateExchangeDTO.getChannel());
      newRateExchangeResult.setCurrencyDestination(newRateExchangeDTO.getCurrencyDestination());
      newRateExchangeResult.setCurrencyOrigin(newRateExchangeDTO.getCurrencyOrigin());
      newRateExchangeResult.setDateTimeUpdate(newRateExchangeDTO.getDateTimeUpdate());
      newRateExchangeResult.setDescription(newRateExchangeDTO.getDescription());
      currencyExchangeDAO.save(currencyExchange);
      singleSubscriber.onSuccess(newRateExchangeResult);
    });
  }
}
