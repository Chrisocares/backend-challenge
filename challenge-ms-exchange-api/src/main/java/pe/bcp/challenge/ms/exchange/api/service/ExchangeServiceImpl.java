package pe.bcp.challenge.ms.exchange.api.service;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.bcp.challenge.ms.exchange.api.dto.RequestExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.RequestNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.mapper.ExchangeCurrencyMapper;
import pe.bcp.challenge.ms.exchange.domain.dao.impl.CurrencyExchangeDaoImpl;
import pe.bcp.challenge.ms.exchange.domain.dto.NewRateExchangeDTO;
import pe.bcp.challenge.ms.exchange.domain.dto.RateExchangeDTO;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Service
public class ExchangeServiceImpl implements ExchangeService{

  @Autowired
  private CurrencyExchangeDaoImpl currencyExchangeDao;

  @Autowired
  private ExchangeCurrencyMapper exchangeCurrencyMapper;

  @Override
  public Single<ResponseExchangeCurrency> calculateRateExchange(RequestExchangeCurrency requestExchangeCurrency) {
    RateExchangeDTO rateExchangeDTO = new RateExchangeDTO();
    rateExchangeDTO.setAmountExchange(requestExchangeCurrency.getAmount());
    rateExchangeDTO.setChannel("WEB");
    rateExchangeDTO.setCurrencyDestination(requestExchangeCurrency.getCurrencyDestination());
    rateExchangeDTO.setCurrencyOrigin(requestExchangeCurrency.getCurrencyOrigin());
    return currencyExchangeDao.calculateExchangeRate(rateExchangeDTO)
      .flatMap(result -> exchangeCurrencyMapper.mapperToResponse(result)).subscribeOn(Schedulers.io());
  }

  @Override
  public Single<ResponseNewRateExchange> createRateExchange(RequestNewRateExchange requestNewRateExchange) {
    String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    NewRateExchangeDTO newRateExchangeDTO = new NewRateExchangeDTO();
    newRateExchangeDTO.setAmountExchange(requestNewRateExchange.getAmountExchange());
    newRateExchangeDTO.setChannel(requestNewRateExchange.getChannel());
    newRateExchangeDTO.setCurrencyDestination(requestNewRateExchange.getCurrencyDestination());
    newRateExchangeDTO.setCurrencyOrigin(requestNewRateExchange.getCurrencyOrigin());
    newRateExchangeDTO.setDateTimeUpdate(timeStamp);
    newRateExchangeDTO.setDescription(requestNewRateExchange.getDescription());
    newRateExchangeDTO.setStrategy(requestNewRateExchange.getStrategy());
    newRateExchangeDTO.setSymbolCurrency(requestNewRateExchange.getSymbolCurrencyDestination());
    return currencyExchangeDao.createNewExchangeRate(newRateExchangeDTO)
      .flatMap(result -> exchangeCurrencyMapper.mapperToResponseNewRate(result)).subscribeOn(Schedulers.io());
  }
}
