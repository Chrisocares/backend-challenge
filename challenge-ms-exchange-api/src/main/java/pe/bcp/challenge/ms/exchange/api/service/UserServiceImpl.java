package pe.bcp.challenge.ms.exchange.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pe.bcp.challenge.ms.exchange.domain.dao.UserDao;
import pe.bcp.challenge.ms.exchange.domain.domain.User;

import java.util.Arrays;
import java.util.List;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService {

  private static final String USER_NOT_FOUND = "Usuario o password invalido";

  @Autowired
  private UserDao userDao;

  @Override
  public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
    User user = userDao.findByUsername(userId);
    if(user == null){
      throw new UsernameNotFoundException(USER_NOT_FOUND);
    }
    return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
  }

  private List<SimpleGrantedAuthority> getAuthority() {
    return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
  }
}
