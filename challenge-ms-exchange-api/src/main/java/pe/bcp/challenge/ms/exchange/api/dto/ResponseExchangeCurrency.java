package pe.bcp.challenge.ms.exchange.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseExchangeCurrency {

  private double amountOrigin;
  private double amountExchange;
  private String currencyOrigin;
  private String currencyDestination;
  private double rateExchange;

}
