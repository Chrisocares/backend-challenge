package pe.bcp.challenge.ms.exchange.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "pe.bcp.challenge")
@EnableJpaRepositories(basePackages = "pe.bcp.challenge")
public class Main {
  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }
}
