package pe.bcp.challenge.ms.exchange.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class RequestNewRateExchange {
  private String channel;
  private String currencyOrigin;
  private String currencyDestination;
  private String description;
  private String strategy;
  private String symbolCurrencyDestination;
  private Double amountExchange;
}
