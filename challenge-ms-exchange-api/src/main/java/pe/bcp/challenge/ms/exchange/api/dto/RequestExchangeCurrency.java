package pe.bcp.challenge.ms.exchange.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestExchangeCurrency {

  private String currencyOrigin;
  private String currencyDestination;
  private Double amount;

}
