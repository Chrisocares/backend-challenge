package pe.bcp.challenge.ms.exchange.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pe.bcp.challenge.ms.exchange.api.dto.BaseWebResponse;
import pe.bcp.challenge.ms.exchange.api.dto.ErrorCode;

import javax.persistence.EntityNotFoundException;

@RestControllerAdvice
public class ExceptionRestController {
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<BaseWebResponse> handleEntityNotFoundException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(BaseWebResponse.error(ErrorCode.INVALID_REQUEST));
    }
}
