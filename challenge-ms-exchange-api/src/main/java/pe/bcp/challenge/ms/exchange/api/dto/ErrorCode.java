package pe.bcp.challenge.ms.exchange.api.dto;

public enum ErrorCode {
  ENTITY_NOT_FOUND,
  INVALID_REQUEST,
  INTERNAL_ERROR
}
