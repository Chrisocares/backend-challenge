package pe.bcp.challenge.ms.exchange.api.controller;

import io.reactivex.schedulers.Schedulers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.bcp.challenge.ms.exchange.api.dto.RequestExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.RequestNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseExchangeCurrency;
import pe.bcp.challenge.ms.exchange.api.dto.ResponseNewRateExchange;
import pe.bcp.challenge.ms.exchange.api.facade.ExchangeCurrencyFacade;
import io.reactivex.Single;
import pe.bcp.challenge.ms.exchange.api.service.ExchangeService;


@RestController
@RequestMapping("/exchangeCurrency")
@Api(
  tags="Endpoint Tipo de Cambio",
  description="Este controlador tiene la responsabilidad de calcular el tipo de cambio en base a una moneda origen y un monto ingresado")
public class ExchangeCurrencyController {

  private final ExchangeCurrencyFacade exchangeCurrencyFacade;

  public ExchangeCurrencyController(ExchangeCurrencyFacade exchangeCurrencyFacade) {
    this.exchangeCurrencyFacade = exchangeCurrencyFacade;
  }

  @Autowired
  private ExchangeService exchangeService;

  @ApiOperation(value = "Calculo de tipo de cambio")
  @PostMapping(path = "/calculate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<ResponseExchangeCurrency>> calculateRateExchange(@RequestBody RequestExchangeCurrency requestExchangeCurrency) {
    return exchangeCurrencyFacade.calculateExchangeRate(requestExchangeCurrency)
      .subscribeOn(Schedulers.io())
      .map(result -> ResponseEntity.ok(result))
      .onErrorReturn(throwable -> null);
  }

  @ApiOperation(value = "Crear tipo de cambio")
  @PostMapping(path = "/new", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<ResponseNewRateExchange>> createRateExchange(@RequestBody RequestNewRateExchange requestNewRateExchange) {
    return exchangeCurrencyFacade.createNewRateExchangeRate(requestNewRateExchange)
      .subscribeOn(Schedulers.io())
      .map(result -> ResponseEntity.ok(result));
  }
}
