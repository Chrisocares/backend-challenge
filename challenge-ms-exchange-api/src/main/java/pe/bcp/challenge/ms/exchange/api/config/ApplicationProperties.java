package pe.bcp.challenge.ms.exchange.api.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProperties {

  @Getter @Setter
  @Value("${config.security.client-id}")
  private String clientID;

  @Getter @Setter
  @Value("${config.security.client-secret}")
  private String clientSecret;

  @Getter @Setter
  @Value("${config.security.grant-type}")
  private String grantType;

  @Getter @Setter
  @Value("${config.security.access-token-validity-seconds}")
  private Integer accessTokenValidity;

  @Getter @Setter
  @Value("${config.security.refresh-token-validity-seconds}")
  private Integer refreshTokenValidity;
}
